package global

import (
	"github.com/satori/go.uuid"
)

type User struct {
	Id          int64     `json:"id"`
	Guid        uuid.UUID `json:"guid"`
	Name        string    `json:"name"`
	Password    string    `json:"password"`
	User_Type	int    	  `json:"userType"`
	Update 		int64     `json:"update"`
}

type Phone struct {
	Id          int64     `json:"id"`
	Guid        uuid.UUID `json:"guid"`
	User_Id     int64     `json:"userId"`
	Name        string    `json:"name"`
	Update 		int64     `json:"update"`
}

type Synchronisation struct {
	Id              int64  `json:"id"`
	Data_Type		int    `json:"dataType"`
	Data_Identifier string `json:"dataIdentifier"`
	User_Id         int64  `json:"userId"`
	Phone_Id        int64  `json:"phoneId"`
	Update          int64  `json:"update"`
	Timestamp       int64  `json:"timestamp"`
	Action          int    `json:"action"`
}

type GoAppTest struct {
	Entry      string    `json:"entry"`
	Reply      string    `json:"reply"`
}


type UserPassword struct {
	Name        string    `json:"name"`
	Password    string    `json:"password"`
}

// ### RECEIPT_DATA #######################################################################

type Transaction struct {
	TransactionID      string  `json:"transactionID"`
	Store              string  `json:"store"`
	StoreType          string  `json:"storeType"`
	Date               int     `json:"date"`
	DiscountAmount     string  `json:"discountAmount"`
	DiscountPercentage string  `json:"discountPercentage"`
	DiscountText       string  `json:"discountText"`
	ExpenseAbroad      string  `json:"expenseAbroad"`
	ExpenseOnline      string  `json:"expenseOnline"`
	TotalPrice         float32 `json:"totalPrice"`
	ReceiptLocation    string  `json:"receiptLocation"`
	ReceiptProductType string  `json:"receiptProductType"`
}

type Product struct {
	TransactionID   string  `json:"transactionID"`
	Product         string  `json:"product"`
	ProductCategory string  `json:"productCategory"`
	Price           float32 `json:"price"`
	ProductCode     string  `json:"productCode"`
	ProductDate     string  `json:"productDate"`
	ProductGroupID  string  `json:"productGroupID"`
}

type Image struct {
	TransactionID   string  `json:"transactionID"`
	Base64image     string  `json:"base64image"`
}

type ReceiptData struct {
	Synchronisation *Synchronisation `json:"synchronisation"`
	Transaction     *Transaction     `json:"transaction"`
	Products        []Product        `json:"products"`
	Image			*Image           `json:"image"`
}

// ### SEARCH_PRODUCT_DATA #######################################################################

type SearchProduct struct {
	Product   		string  `json:"product"`
	ProductCategory string  `json:"productCategory"`
	ProductCode 	string  `json:"productCode"`
	LastAdded       int 	`json:"lastAdded"`
	Count     		int  	`json:"count"`
}

type SearchProductData struct {
	Synchronisation *Synchronisation 	`json:"synchronisation"`
	SearchProduct   *SearchProduct 		`json:"searchProduct"`
}

// ### SEASRCH_STORE_DATA #######################################################################

type SearchStore struct {
	StoreName   	string  `json:"storeName"`
	StoreType 		string  `json:"storeType"`
	LastAdded       int 	`json:"lastAdded"`
	Count     		int  	`json:"count"`
}

type SearchStoreData struct {
	Synchronisation *Synchronisation 	`json:"synchronisation"`
	SearchStore   	*SearchStore 		`json:"searchStore"`
}

// ##########################################################################