
DROP TABLE public.tbl_receipt_image;
DROP TABLE public.tbl_receipt_product;
DROP TABLE public.tbl_receipt_transaction;
DROP TABLE public.tbl_search_suggestions_products;
DROP TABLE public.tbl_search_suggestions_stores;
DROP TABLE public.tbl_sync;
DROP TABLE public.tbl_phone;
DROP TABLE public.tbl_user;

CREATE TABLE public.tbl_user
(
    id BIGSERIAL PRIMARY KEY,
    guid uuid NOT NULL,
    name text NOT NULL,
    password text NOT NULL,
    user_type integer NOT NULL,
    "update" bigint NOT NULL,
    created date NOT NULL,
    changed date NOT NULL
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tbl_user
    OWNER to postgres;
	
CREATE TABLE public.tbl_phone
(
    id BIGSERIAL PRIMARY KEY,
    guid uuid NOT NULL,
    user_id bigint NOT NULL,
    name text NOT NULL,
    "update" bigint NOT NULL,
    created date NOT NULL,
    changed date NOT NULL,
    CONSTRAINT tbl_phone_user_id_fkey FOREIGN KEY (user_id)
        REFERENCES public.tbl_user (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tbl_phone
    OWNER to postgres;

CREATE TABLE public.tbl_sync
(
    id BIGSERIAL PRIMARY KEY,
    data_type integer NOT NULL,
    data_identifier TEXT NOT NULL,
    user_id bigint NOT NULL,
    phone_id bigint NOT NULL,
    "update" bigint NOT NULL,
    "timestamp" bigint NOT NULL,
    action integer NOT NULL,
    created date NOT NULL,
    changed date NOT NULL,
    CONSTRAINT tbl_sync_phone_id_fkey FOREIGN KEY (phone_id)
        REFERENCES public.tbl_phone (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT tbl_sync_user_id_fkey FOREIGN KEY (user_id)
        REFERENCES public.tbl_user (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tbl_sync
    OWNER to postgres;




CREATE TABLE public.tbl_receipt_transaction 
(
    sync_id bigint NOT NULL,
    transaction_id TEXT,
    store TEXT,
    store_type TEXT,
    "date" int,
    discount_amount TEXT,
    discount_percentage TEXT,
    expense_abroad TEXT,
    expense_online TEXT,
    total_price float,
    discount_text TEXT,
    receipt_location TEXT,
    receipt_producttype TEXT,
    CONSTRAINT tbl_receipt_transaction_sync_id_fkey FOREIGN KEY (sync_id)
        REFERENCES public.tbl_sync (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tbl_receipt_transaction
    OWNER to postgres;



CREATE TABLE public.tbl_receipt_product 
(
    sync_id bigint NOT NULL,
    transaction_id TEXT,
    product TEXT,
    product_category TEXT,
    price float,
    product_code TEXT,
    product_date TEXT,
    product_group_id TEXT,
    CONSTRAINT tbl_receipt_product_sync_id_fkey FOREIGN KEY (sync_id)
        REFERENCES public.tbl_sync (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tbl_receipt_product
    OWNER to postgres;



CREATE TABLE public.tbl_receipt_image 
(
    sync_id bigint NOT NULL,
    transaction_id TEXT,
    "image" bytea,
    CONSTRAINT tbl_receipt_image_sync_id_fkey FOREIGN KEY (sync_id)
        REFERENCES public.tbl_sync (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tbl_receipt_image
    OWNER to postgres;




CREATE TABLE public.tbl_search_suggestions_products
(
    sync_id bigint NOT NULL,
    product TEXT,
    productCategory TEXT,
    productCode TEXT,
    lastAdded bigint NOT NULL,
    "count" bigint NOT NULL,
    CONSTRAINT tbl_search_suggestions_products_sync_id_fkey FOREIGN KEY (sync_id)
        REFERENCES public.tbl_sync (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tbl_search_suggestions_products
    OWNER to postgres;



CREATE TABLE public.tbl_search_suggestions_stores
(
    sync_id bigint NOT NULL,
    storeName TEXT,
    storeType TEXT,
    lastAdded bigint NOT NULL,
    "count" bigint NOT NULL,
    CONSTRAINT tbl_search_suggestions_stores_sync_id_fkey FOREIGN KEY (sync_id)
        REFERENCES public.tbl_sync (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tbl_search_suggestions_stores
    OWNER to postgres;



