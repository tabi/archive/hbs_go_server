package database

import (
	"database/sql"
	"errors"
	"github.com/satori/go.uuid"
	"time"

	_ "github.com/lib/pq"

	"budget/global"
)

func createUser(user global.User) (int64, error) {
	sqlStatement := "" +
		"INSERT INTO " + global.TBL_USER + " (guid, name, password, user_type, update, created, changed) " +
		"VALUES ($1, $2, $3, $4, $5, $6, $7) " +
		"RETURNING id "

	var id int64 = -1
	var now = time.Now()

	row := con.QueryRow(sqlStatement, user.Guid, user.Name, user.Password, user.User_Type, user.Update, now, now)
	switch err = row.Scan(&id); err {
	case nil:
		//global.MyPrint("& Created User")
		return id, nil
	default:
		return id, err
	}
}

func updateUserUpdate(tx *sql.Tx, user global.User) error {
	sqlStatement := "" +
		"UPDATE " + global.TBL_USER + " " +
		"SET update=$1, changed=$2 " +
		"WHERE id=$3 "

	var now = time.Now()

	_, err = tx.Exec(sqlStatement, user.Update, now, user.Id)
	if err != nil {
		return err
	}

	global.MyPrint("& Updated User Update")
	return nil
}

func updateUserPassword(user global.User) error {
	sqlStatement := "" +
		"UPDATE " + global.TBL_USER + " " +
		"SET password=$1, changed=$2 " +
		"WHERE id=$3 "

	var now = time.Now()

	_, err = con.Exec(sqlStatement, user.Password, now, user.Id)
	if err != nil {
		return err
	}

	//global.MyPrint("& Updated User Password")
	return nil
}

func updateUserType(user global.User) error {
	sqlStatement := "" +
		"UPDATE " + global.TBL_USER + " " +
		"SET user_type=$1, changed=$2 " +
		"WHERE id=$3 "

	var now = time.Now()

	_, err = con.Exec(sqlStatement, user.User_Type, now, user.Id)
	if err != nil {
		return err
	}

	//global.MyPrint("& Updated User Type")
	return nil
}

func scanUserRow(rows *sql.Rows) global.User {
	var id int64
	var guid uuid.UUID
	var name string
	var password string
	var user_type int
	var update int64
	err = rows.Scan(&id, &guid, &name, &password, &user_type, &update)
	if err != nil {
		panic(err)
	}
	return global.User{
		Id:         id,
		Guid:       guid,
		Name:       name,
		Password:   password,
		User_Type:	user_type,
		Update: 	update}
}

func selectUser_ByGuid(guid uuid.UUID) (global.User, error) {
	var user global.User

	sqlStatement := "" +
		"SELECT id, guid, name, password, user_type, update " +
		"FROM " + global.TBL_USER + " " +
		"WHERE guid=$1 LIMIT 1 "

	rows, err := con.Query(sqlStatement, guid)
	if err != nil {
		return user, err
	}
	defer rows.Close()

	var found bool = false
	for rows.Next() {
		user = scanUserRow(rows)
		found = true
		break
	}
	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		return user, err
	}

	if found {
		return user, nil
	} else {
		return user, errors.New("Error: User not found")
	}
}

func selectUser_ByName(name string) (global.User, error) {
	var user global.User

	sqlStatement := "" +
		"SELECT id, guid, name, password, user_type, update " +
		"FROM " + global.TBL_USER + " " +
		"WHERE name=$1 LIMIT 1 "

	rows, err := con.Query(sqlStatement, name)
	if err != nil {
		return user, err
	}
	defer rows.Close()

	var found bool = false
	for rows.Next() {
		user = scanUserRow(rows)
		found = true
		break
	}
	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		return user, err
	}

	if found {
		return user, nil
	} else {
		return user, errors.New("Error: User not found")
	}
}

func selectUser_ById(id int64) (global.User, error) {
	var user global.User

	sqlStatement := "" +
		"SELECT id, guid, name, password, user_type, update " +
		"FROM " + global.TBL_USER + " " +
		"WHERE id=$1 LIMIT 1 "

	rows, err := con.Query(sqlStatement, id)
	if err != nil {
		return user, err
	}
	defer rows.Close()

	var found bool = false
	for rows.Next() {
		user = scanUserRow(rows)
		found = true
		break
	}
	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		return user, err
	}

	if found {
		return user, nil
	} else {
		return user, errors.New("Error: User not found")
	}
}
