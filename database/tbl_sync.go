package database

import (
	"database/sql"
	"errors"
	"time"

	_ "github.com/lib/pq"

	"budget/global"
)

func createSynchronisation(tx *sql.Tx, synchronisation *global.Synchronisation) (int64, error) {
	sqlStatement := "" +
		"INSERT INTO " + global.TBL_SYNC + " (data_type, data_identifier, user_id, phone_id, update, timestamp, action, created, changed) " +
		"VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9) " +
		"RETURNING id "

	var id int64 = -1
	var now = time.Now()

	row := tx.QueryRow(sqlStatement,
		synchronisation.Data_Type,
		synchronisation.Data_Identifier,
		synchronisation.User_Id,
		synchronisation.Phone_Id,
		synchronisation.Update,
		synchronisation.Timestamp,
		synchronisation.Action,
		now,
		now)
	switch err = row.Scan(&id); err {
	case nil:
		global.MyPrint("& Inserted Sync")
		return id, nil
	default:
		return id, err
	}
}

func updateSynchronisation(tx *sql.Tx, synchronisation *global.Synchronisation) (bool, error) {
	sqlStatement := "" +
		"UPDATE " + global.TBL_SYNC + " " +
		"SET phone_id=$1, update=$2, timestamp=$3, action=$4, changed=$5 " +
		"WHERE id=$6 AND timestamp<$7 "

	var now = time.Now()

	result, err := tx.Exec(sqlStatement,
		synchronisation.Phone_Id,
		synchronisation.Update,
		synchronisation.Timestamp,
		synchronisation.Action,
		now,
		synchronisation.Id,
		synchronisation.Timestamp)
	if err != nil {
		return false, err
	}

	var affected int64
	affected, err = result.RowsAffected()
	if affected == 1 {
		global.MyPrint("& Updated Sync")
		return true, nil
	} else {
		return false, errors.New("Error: Could not update Sync!")
	}
}

func scanSynchronisationRow(rows *sql.Rows) global.Synchronisation {
	var id int64
	var data_type int
	var data_identifier string
	var user_id int64
	var phone_id int64
	var update int64
	var timestamp int64
	var action int
	err = rows.Scan(&id, &data_type, &data_identifier, &user_id, &phone_id, &update, &timestamp, &action)
	if err != nil {
		panic(err)
	}
	return global.Synchronisation{
		Id:              id,
		Data_Type:		 data_type,
		Data_Identifier: data_identifier,
		User_Id:         user_id,
		Phone_Id:        phone_id,
		Update:          update,
		Timestamp:       timestamp,
		Action:          action}
}

func selectSynchronisation_NextToPull(data_type int, user_id int64, phone_id int64, pulled_update int64) (global.Synchronisation, error) {
	var synchronisation global.Synchronisation

	sqlStatement := "" +
		"SELECT id, data_type, data_identifier, user_id, phone_id, update, timestamp, action " +
		"FROM " + global.TBL_SYNC + " " +
		"WHERE data_type=$1 AND user_id=$2 AND phone_id<>$3 AND update>$4 " +
		"ORDER BY update "

	rows, err := con.Query(sqlStatement, data_type, user_id, phone_id, pulled_update)
	if err != nil {
		return synchronisation, err
	}
	defer rows.Close()

	var found bool = false
	for rows.Next() {
		synchronisation = scanSynchronisationRow(rows)
		found = true
		break
	}

	err = rows.Err()
	if err != nil {
		return synchronisation, err
	}

	if found {
		return synchronisation, nil
	} else {
		return synchronisation, errors.New("Error: Next Sync not found!")
	}
}

func selectSynchronisation_ByDataIdentifier(tx *sql.Tx, data_identifier string, user_id int64) (global.Synchronisation, error) {
	var synchronisation global.Synchronisation

	sqlStatement := "" +
		"SELECT id, data_type, data_identifier, user_id, phone_id, update, timestamp, action " +
		"FROM " + global.TBL_SYNC + " " +
		"WHERE data_identifier=$1 AND user_id=$2 "

	rows, err := tx.Query(sqlStatement, data_identifier, user_id)
	if err != nil {
		return synchronisation, err
	}
	defer rows.Close()

	var found bool = false
	for rows.Next() {
		synchronisation = scanSynchronisationRow(rows)
		found = true
		break
	}

	err = rows.Err()
	if err != nil {
		return synchronisation, err
	}

	if found {
		return synchronisation, nil
	} else {
		return synchronisation, errors.New("Error: Sync DataIdentifier not found!")
	}
}

func selectSynchronisation_ById(id int64) (global.Synchronisation, error) {
	var synchronisation global.Synchronisation

	sqlStatement := "" +
		"SELECT id, data_type, data_identifier, user_id, phone_id, update, timestamp, action " +
		"FROM " + global.TBL_SYNC + " " +
		"WHERE id=$1 "

	rows, err := con.Query(sqlStatement, id)
	if err != nil {
		return synchronisation, err
	}
	defer rows.Close()

	var found bool = false
	for rows.Next() {
		synchronisation = scanSynchronisationRow(rows)
		found = true
		break
	}

	err = rows.Err()
	if err != nil {
		return synchronisation, err
	}

	if found {
		return synchronisation, nil
	} else {
		return synchronisation, errors.New("Error: Sync Id not found!")
	}
}
