package database

import (
	"database/sql"
	"errors"
	_ "github.com/lib/pq"
	"github.com/satori/go.uuid"
	"time"

	"budget/global"
)

func createPhone(phone global.Phone) (int64, error) {
	sqlStatement := "" +
		"INSERT INTO " + global.TBL_PHONE + " (guid, user_id, name, update, created, changed) " +
		"VALUES ($1, $2, $3, $4, $5, $6) " +
		"RETURNING id "

	var id int64 = -1
	var now = time.Now()

	row := con.QueryRow(sqlStatement, phone.Guid, phone.User_Id, phone.Name, phone.Update, now, now)
	switch err = row.Scan(&id); err {
	case nil:
		global.MyPrint("& Inserted Phone")
		return id, nil
	default:
		return id, err
	}
}

func updatePhone(tx *sql.Tx, phone global.Phone) error {
	sqlStatement := "" +
		"UPDATE " + global.TBL_PHONE + " " +
		"SET name=$1, update=$2, changed=$3 " +
		"WHERE id=$4 "

	var now = time.Now()

	_, err = tx.Exec(sqlStatement, phone.Name, phone.Update, now, phone.Id)
	if err != nil {
		return err
	}

	global.MyPrint("& Updated Phone")
	return nil
}

func scanPhoneRow(rows *sql.Rows) global.Phone {
	var id int64
	var guid uuid.UUID
	var user_id int64
	var name string
	var update int64
	err = rows.Scan(&id, &guid, &user_id, &name, &update)
	if err != nil {
		panic(err)
	}
	return global.Phone{
		Id:         id,
		Guid:       guid,
		User_Id:    user_id,
		Name:       name,
		Update: 	update}
}

func selectPhone_ByGuid(guid uuid.UUID, user_id int64) (global.Phone, error) {
	var phone global.Phone

	sqlStatement := "" +
		"SELECT id, guid, user_id, name, update " +
		"FROM " + global.TBL_PHONE + " " +
		"WHERE guid=$1 AND user_id=$2 LIMIT 1 "

	rows, err := con.Query(sqlStatement, guid, user_id)
	if err != nil {
		return phone, err
	}
	defer rows.Close()

	var found bool = false
	for rows.Next() {
		phone = scanPhoneRow(rows)
		found = true
		break
	}
	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		return phone, err
	}

	if found {
		return phone, nil
	} else {
		return phone, errors.New("Error: Phone not found!")
	}
}

func selectPhone_ByName(phoneName string, user_id int64) (global.Phone, error) {
	var phone global.Phone

	sqlStatement := "" +
		"SELECT id, guid, user_id, name, update " +
		"FROM " + global.TBL_PHONE + " " +
		"WHERE name=$1 AND user_id=$2 LIMIT 1 "

	rows, err := con.Query(sqlStatement, phoneName, user_id)
	if err != nil {
		return phone, err
	}
	defer rows.Close()

	var found bool = false
	for rows.Next() {
		phone = scanPhoneRow(rows)
		found = true
		break
	}

	err = rows.Err()
	if err != nil {
		return phone, err
	}

	if found {
		return phone, nil
	} else {
		return phone, errors.New("Error: Phone not found!")
	}
}

func selectPhone_ById(id int64) (global.Phone, error) {
	var phone global.Phone

	sqlStatement := "" +
		"SELECT id, guid, user_id, name, update " +
		"FROM " + global.TBL_PHONE + " " +
		"WHERE id=$1 LIMIT 1 "

	rows, err := con.Query(sqlStatement, id)
	if err != nil {
		return phone, err
	}
	defer rows.Close()

	var found bool = false
	for rows.Next() {
		phone = scanPhoneRow(rows)
		found = true
		break
	}

	err = rows.Err()
	if err != nil {
		return phone, err
	}

	if found {
		return phone, nil
	} else {
		return phone, errors.New("Error: Phone not found!")
	}
}

func selectPhones_ByUserId(user_id int64) ([]global.Phone, error) {
	var phones []global.Phone

	sqlStatement := "" +
		"SELECT id, guid, user_id, name, update " +
		"FROM " + global.TBL_PHONE + " " +
		"WHERE user_id=$1 "

	rows, err := con.Query(sqlStatement, user_id)
	if err != nil {
		return phones, err
	}
	defer rows.Close()

	var found bool = false
	for rows.Next() {
		found = true
		phones = append(phones, scanPhoneRow(rows))
	}
	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		return phones, err
	}

	if found {
		return phones, nil
	} else {
		return phones, errors.New("Error: Phone not found!")
	}
}
