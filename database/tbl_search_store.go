package database

import (
	"database/sql"
	"errors"

	_ "github.com/lib/pq"

	"budget/global"
)

func createSearchStore(tx *sql.Tx, sync_id int64, s *global.SearchStore) error {
	sqlStatement := "" +
		"INSERT INTO " + global.TBL_SEARCH_STORE + " (sync_id, storeName, storeType, " +
		"	lastAdded, count) " +
		"VALUES ($1, $2, $3, $4, $5) "

	_, err = tx.Exec(sqlStatement, sync_id, s.StoreName, s.StoreType, s.LastAdded, s.Count)
	if err != nil {
		return err
	}

	global.MyPrint("& Inserted SearchStore")
	return nil
}

func deleteSearchStore(tx *sql.Tx, sync_id int64) error {
	sqlStatement := "" +
		"DELETE FROM " + global.TBL_SEARCH_STORE + " " +
		"WHERE sync_id=$1 "

	_, err = tx.Exec(sqlStatement, sync_id)
	if err != nil {
		return err
	}
	
	global.MyPrint("& Deleted SearchStore")
	return nil
}

func existsSearchStore(sync_id int64) (bool, error) {
	var exists bool

	sqlStatement := "" +
		"SELECT EXISTS " +
		"(SELECT sync_id FROM " + global.TBL_SEARCH_STORE + " WHERE sync_id=$1) "

	err := con.QueryRow(sqlStatement, sync_id).Scan(&exists)
	if err != nil && err != sql.ErrNoRows {
		return false, err
	}
	return exists, nil
}

func scanSearchStoreRow(rows *sql.Rows) global.SearchStore {
	var sync_id int64
	var storeName string
	var storeType string
	var lastAdded int
	var count int
	err = rows.Scan(&sync_id, &storeName, &storeType, &lastAdded, &count)
	if err != nil {
		panic(err)
	}
	return global.SearchStore{
		StoreName:    	storeName,
		StoreType:    	storeType,
		LastAdded:    	lastAdded,
		Count:      	count}
}

func selectSearchStore(sync_id int64) (global.SearchStore, error) {
	var searchStore global.SearchStore

	sqlStatement := "" +
		"SELECT sync_id, storeName, storeType, " +
		"	lastAdded, count " +
		"FROM " + global.TBL_SEARCH_STORE + " " +
		"WHERE sync_id=$1 "

	rows, err := con.Query(sqlStatement, sync_id)
	if err != nil {
		return searchStore, err
	}
	defer rows.Close()

	var found bool = false
	for rows.Next() {
		searchStore = scanSearchStoreRow(rows)
		found = true
		break
	}
	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		return searchStore, err
	}

	if found {
		return searchStore, nil
	} else {
		return searchStore, errors.New("Error: SearchStore not found!")
	}
}
