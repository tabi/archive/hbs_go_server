package database

import (
	"database/sql"
	"errors"

	_ "github.com/lib/pq"

	"budget/global"
)

func createProduct(tx *sql.Tx, sync_id int64, p *global.Product) error {
	sqlStatement := "" +
		"INSERT INTO " + global.TBL_PRODUCT + " (sync_id, transaction_id, product, product_category, " +
		"	price, product_code, product_date, product_group_id) " +
		"VALUES ($1, $2, $3, $4, $5, $6, $7, $8) "

	_, err = tx.Exec(sqlStatement, sync_id,
		p.TransactionID, p.Product, p.ProductCategory, p.Price, p.ProductCode, p.ProductDate, p.ProductGroupID)
	if err != nil {
		return err
	}

	global.MyPrint("& Inserted Product")
	return nil
}

func deleteProducts(tx *sql.Tx, sync_id int64) error {
	sqlStatement := "" +
		"DELETE FROM " + global.TBL_PRODUCT + " " +
		"WHERE sync_id=$1 "

	_, err = tx.Exec(sqlStatement, sync_id)
	if err != nil {
		return err
	}

	global.MyPrint("& Deleted Product")
	return nil
}

func existsProducts(sync_id int64) (bool, error) {
	var exists bool

	sqlStatement := "" +
		"SELECT EXISTS " +
		"(SELECT sync_id FROM " + global.TBL_PRODUCT + " WHERE sync_id=$1) "

	err := con.QueryRow(sqlStatement, sync_id).Scan(&exists)
	if err != nil && err != sql.ErrNoRows {
		return false, err
	}
	return exists, nil
}

func scanProductRow(rows *sql.Rows) global.Product {
	var sync_id int64
	var transactionID string
	var product string
	var productCategory string
	var price float32
	var productCode string
	var productDate string
	var productGroupID string
	err = rows.Scan(&sync_id, &transactionID, &product, &productCategory,
		&price, &productCode, &productDate, &productGroupID)
	if err != nil {
		panic(err)
	}
	return global.Product{
		TransactionID:   transactionID,
		Product:         product,
		ProductCategory: productCategory,
		Price:           price,
		ProductCode:     productCode,
		ProductDate:     productDate,
		ProductGroupID:	 productGroupID}
}

func selectProducts(sync_id int64) ([]global.Product, error) {
	var products []global.Product

	sqlStatement := "" +
		"SELECT sync_id, transaction_id, product, product_category, " +
		"	price, product_code, product_date, product_group_id " +
		"FROM " + global.TBL_PRODUCT + " " +
		"WHERE sync_id=$1 "

	rows, err := con.Query(sqlStatement, sync_id)
	if err != nil {
		return products, err
	}
	defer rows.Close()

	var found bool = false
	for rows.Next() {
		products = append(products, scanProductRow(rows))
		found = true
	}
	// get any error encountered during iteration
	err = rows.Err()
	if err != nil {
		return products, err
	}

	if found {
		return products, nil
	} else {
		return products, errors.New("Error: Products not found!")
	}
}
